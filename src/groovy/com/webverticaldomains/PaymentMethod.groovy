package com.webverticaldomains

public enum PaymentMethod {
    CASH, NET_BANKING, CREDIT_CARD, DEBIT_CARD, PAYPAL
}

package com.webverticaldomains



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(ApplicationTagLib)
class ApplicationTagLibTests {

    void testSomething() {
        fail "Implement me"
    }
}

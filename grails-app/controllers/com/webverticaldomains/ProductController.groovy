package com.webverticaldomains


import grails.converters.JSON
import org.codehaus.groovy.grails.web.util.StreamCharBuffer

import javax.servlet.http.Cookie

class ProductController {

    def applicationService

    def index(){
        List <Product> products = Product.list()
        render(view: '/index', model: [products:products])
    }

    def details(){
        def productId = params.getLong('id', -1)
        def product
        if (productId > -1)
            product = Product.get(productId)
        render(view: 'details', model: [product: product])
    }

    def getImage() {
        def imageId = 0
        def productImage = Image.get(imageId)

        if(! productImage){
            response.status(404, "Image not found")
            return
        }

        response.setHeader('Content-type', productImage.type)
        response.outputStream << productImage.file
        response.flush()
    }

    def productImage() {
        def productId = params.getLong('productId', -1)
        def imageId = params.getLong('imageId', -1)
        if (productId!=-1) {
            def product = Product.get(productId)
            if (product?.images?.size()>0) {
                imageId = product.images.find {
                    it.id==imageId
                }?.id?:(product.images.size()>imageId?product.images.getAt(imageId):imageId)
            }
        }
        Image productImage
        if (imageId!=-1)
            productImage = Image.get(imageId)
        if (! productImage) {
            response.sendError(404, 'Image Not Found')
            return
        }

        response.addHeader("Content-Disposition", "inline; filename=\"${productImage.name}\"")
        response.addHeader("Content-Type", productImage.type)
        response.outputStream << productImage.file
        response.outputStream.flush()
    }

    def addReview() {
        def product = Product.get(params.getLong('productId'))
        def review = new Review(params)
        product.addToReviews(review)
        product.save()
        redirect(action: 'details', id: product.id)
    }

    def listProducts() {
        def cat = Category.get(params.catId as long)
        def products = Product.findAllByCategory(cat)
        render(view: 'listProducts', model: [products: products])
    }

    def addToCart() {
        def cartList = cookie(name: 'cartList')?:[]
        if (cartList instanceof StreamCharBuffer)
            cartList = JSON.parse(cartList as String) as List
        def productId = params.getLong('id', -1)
        def cartItem = cartList.find() {
            (productId==it.productId)
        }
        if (!cartItem)
            cartList << [productId:(params.id as long),quantity:(params.quantity as int)]
        else
            cartItem << [productId:(params.id as long),quantity:(params.quantity as int)]
        def cartListCookie = new Cookie('cartList',(cartList as JSON).toString())
        cartListCookie.maxAge = -1
        response.addCookie(cartListCookie)
        render([success: true] as JSON)
    }

    def showCart() {
         def cartList = cookie(name:  'cartList')?:[]
         if (cartList instanceof StreamCharBuffer)
            cartList = JSON.parse(cartList as String) as List
         def products = cartList.collect { [product: Product.get(it.productId), quantity: it.quantity] } as List
         render(view: 'showCart',model: [products: products])
    }
}

package com.webverticaldomains

import com.webverticaldomains.auth.Role
import com.webverticaldomains.auth.User
import com.webverticaldomains.auth.UserRole
import grails.converters.JSON

class SecureController {

    def index() {
        //render 'security access only'
        render view: 'index'
    }

    def createUser() {
        render(view: 'registerUser')
    }
    def enterCategory(){
        render(view: 'saveCategory')
    }

    def saveCategory(){
        def categoryInstance = new Category(name: params.name)
        if (params.actualParentCat) {
            def parentCategory = Category.get(params.actualParentCat as long)
            categoryInstance.parentCategory = parentCategory
        }
        categoryInstance = categoryInstance.save()
        flash.message = "Category <b>$categoryInstance.name</b> successfully created."
        redirect(action: 'enterCategory')
    }

    def createSubCat() {
        def category = Category.get(params.catId as long)
        def subCats = Category.findAllByParentCategory(category)
        if(subCats?.size())
            render(template: 'subCategory', model: [subCats: subCats, catId: params.catId])
        else
            render(text: '')
    }

    def registerUser() {
        def isAjax = params.getBoolean('ajaxRequest', false)
        def userInstance = new User(
            username:  params.username,
            password:  params.password,
            email:  params.email
        )
        userInstance.save()
        if (userInstance?.hasErrors()) {
            if (isAjax) {
                def errors = [:]
                userInstance.errors.fieldErrors.each { errors << [(it.field): it.defaultMessage] }
                render([errors: errors, success: false] as JSON)
            } else {
                flash.error = userInstance.errors.fieldErrors.each { "${it.field}: ${it.defaultMessage}" }.join("<br />")
                render(view: 'registerUser')
            }
        } else if (userInstance?.id) {
            UserRole.create userInstance, Role.findByAuthority("ROLE_USER"), true
            if (isAjax)
                render([message: "Please check your email for activation email", success: true] as JSON)
            else
                redirect(controller: 'login', action: 'auth')
        }
    }

    def createProduct() {
        render(view: 'createProduct')
    }

    def saveProduct() {
        def category = Category.get(params.actualParentCat as long)
        def product = new Product(category: category)
        bindData(product, params)
        if (! product.validate()) {
            flash.error = product.errors.fieldErrors.each { "${it.field}: ${it.defaultMessage}" }.join("<br />")
            redirect(action: 'createProduct')
            return
        }
        product.save(validate: false, flush: true)
        def files = request.getFiles("productImages")
        if(files?.size() && files[0].fileItem.size > 0) {
            files?.each {
                def file = new Image(file: it, name: it.originalFilename, type: it.contentType)
                if (file.save())
                    product.addToImages(file)
            }
        }
        redirect(action: 'createProduct')
    }
}

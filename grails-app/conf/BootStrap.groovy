import com.webverticaldomains.auth.Role
import com.webverticaldomains.auth.User
import com.webverticaldomains.auth.UserRole

class BootStrap {

    def init = { servletContext ->

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        def testUser = new User(username: 'me', password: 'password', email: 'abcd@xyz.com')
        testUser =  testUser.save(flush: true)

        UserRole.create testUser, adminRole, true
    }

}

class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		/*"/"(view:"/index")*/
        "/"(controller: 'product', action: 'index')
		"500"(view:'/error')
        "/index"(controller: 'product', action: 'index')
	}
}

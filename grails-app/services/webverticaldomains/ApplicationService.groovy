package webverticaldomains

import com.webverticaldomains.Billing
import com.webverticaldomains.Product
import com.webverticaldomains.auth.User

class ApplicationService {

    def springSecurityService

    public boolean isEligibleForReview(User user, Product product) {
        if (!user) {
            user = springSecurityService.currentUser
        }
        Billing.findByCreatedByAndProductsInList(user, [product]) != null
    }
}

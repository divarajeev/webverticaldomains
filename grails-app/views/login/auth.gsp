<html>
<head>
	<meta name='layout' content='splash'/>
	<title>Login</title>
</head>

<body>
<div class="login-layout">
    <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        <div class="center">
                            <h1>
                                <i class="icon-leaf green"></i>
                                <span class="red">Store</span>
                                <span class="white">Manager</span>
                            </h1>
                            <h4 class="blue">&copy; XYZ Solutions</h4>
                        </div>

                        <div class="space-6"></div>

                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border" style="margin-top:20%">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="icon-coffee green"></i>
                                            Please Enter Your Information
                                        </h4>

                                        <div class="space-6"></div>

                                        <g:form url='${postUrl}' method='POST' name='loginForm'>
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" class="form-control" name='j_username' id='username' placeholder="Username" />
                                                        %{--<input type='text' class='input-large text_' name='j_username' id='username'placeholder="Username"/>--}%
                                                        <i class="icon-user"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="password" class="form-control" name='j_password' id='password' placeholder="Password" />
                                                        %{-- <input type='password' class='input-large text_' name='j_password' id='password' placeholder="Password"/>--}%
                                                        <i class="icon-lock"></i>
                                                    </span>
                                                </label>

                                                <div class="space"></div>

                                                <div class="clearfix">
                                                    <label class="inline">
                                                        <input type="checkbox" class="ace" />
                                                        <span class="lbl"> Remember Me</span>
                                                    </label>

                                                    <button type="submit" id="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                        <i class="icon-key"></i>
                                                        Login
                                                    </button>
                                                </div>

                                                <div class="space-4"></div>

                                                <g:if test='${flash.message}'>
                                                    <div class='login_message' style='color:red'>${flash.message}</div>
                                                </g:if>

                                            </fieldset>
                                        </g:form>
                                    </div><!-- /widget-main -->

                                    <div class="toolbar clearfix">
                                        <div>
                                            <a href="#" onclick="show_box('forgot-box'); return false;" class="forgot-password-link">
                                                <i class="icon-arrow-left"></i>
                                                I forgot my password
                                            </a>
                                        </div>

                                        <div>
                                            <a href="#" onclick="show_box('signup-box'); return false;" class="user-signup-link">
                                                I want to register
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div><!-- /widget-body -->
                            </div><!-- /login-box -->

                            <div id="forgot-box" class="forgot-box widget-box no-border" style="margin-top:20%">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header red lighter bigger">
                                            <i class="icon-key"></i>
                                            Retrieve Password
                                        </h4>

                                        <div class="space-6"></div>
                                        <p>
                                            Enter your email and to receive instructions
                                        </p>

                                        <form>
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="email" class="form-control" placeholder="Email" />
                                                        <i class="icon-envelope"></i>
                                                    </span>
                                                </label>

                                                <div class="clearfix">
                                                    <button type="button" class="width-35 pull-right btn btn-sm btn-danger">
                                                        <i class="icon-lightbulb"></i>
                                                        Send Me!
                                                    </button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div><!-- /widget-main -->

                                    <div class="toolbar center">
                                        <a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
                                            Back to login
                                            <i class="icon-arrow-right"></i>
                                        </a>
                                    </div>
                                </div><!-- /widget-body -->
                            </div><!-- /forgot-box -->

                            <div id="signup-box" class="signup-box widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header green lighter bigger">
                                            <i class="icon-group blue"></i>
                                            New User Registration
                                        </h4>

                                        <div class="space-6"></div>
                                        <p> Enter your details to begin: </p>

                                        <g:formRemote name="registerForm" url="[controller: 'secure', action: 'registerUser', params:[ajaxRequest: true]]" method="post" onSuccess="handleRegisterResponse(data)">
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input name="email" type="email" class="form-control" placeholder="Email" />
                                                        <i class="icon-envelope"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <g:textField name="username" class="form-control" placeholder="Username" />
                                                        <i class="icon-user"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <g:passwordField name="password" class="form-control" placeholder="Password" />
                                                        <i class="icon-lock"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <g:passwordField name="confirmPassword" class="form-control" placeholder="Repeat password" />
                                                        <i class="icon-retweet"></i>
                                                    </span>
                                                </label>

                                                <label class="block">
                                                    <g:checkBox name="userAgreement" class="ace" />
                                                    <span class="lbl">
                                                        I accept the
                                                        <a href="#">User Agreement</a>
                                                    </span>
                                                </label>

                                                <div class="space-24"></div>

                                                <div class="clearfix">
                                                    <button type="reset" name="reset" class="width-30 pull-left btn btn-sm">
                                                        <i class="icon-refresh"></i>
                                                        Reset
                                                    </button>

                                                    <button type="submit" id="register" name="register" class="width-65 pull-right btn btn-sm btn-success">
                                                        Register
                                                        <i class="icon-arrow-right icon-on-right"></i>
                                                    </button>
                                                </div>
                                            </fieldset>
                                        </g:formRemote>
                                    </div>

                                    <div class="toolbar center">
                                        <a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
                                            <i class="icon-arrow-left"></i>
                                            Back to login
                                        </a>
                                    </div>
                                </div><!-- /widget-body -->
                            </div><!-- /signup-box -->


                        </div><!-- /position-relative -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div><!-- /.main-container -->





%{--
</div>
--}%

<script type="text/javascript">
    function show_box(id) {
        jQuery('.widget-box.visible').removeClass('visible');
        jQuery('#'+id).addClass('visible');
    }
</script>


<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
    $('#register').click(validateRegister);
    function validateRegister(evt) {
        var formElem = $("form#registerForm");
        var passwordElem = formElem.find("#password");
        var confirmPasswordElem = formElem.find("#confirmPassword");
        var userAgreement = formElem.find("#userAgreement");

        passwordElem.parent().removeClass("has-error");
        passwordElem.parent().parent().find("i.red").remove();
        confirmPasswordElem.parent().removeClass("has-error");
        confirmPasswordElem.parent().parent().find("i.red").remove();
        userAgreement.parent().removeClass("has-error");
        userAgreement.parent().parent().find("i.red").remove();

        var hasAgreed = formElem.find("#userAgreement:checked").size() > 0;
        if(passwordElem.val().length>=8 && passwordElem.val()==confirmPasswordElem.val() && hasAgreed)
            return;

        evt.preventDefault();
        evt.stopPropagation();
        if(passwordElem.val().length<8) {
            passwordElem.parent().addClass("has-error");
            passwordElem.parent().after("<i class='red'>Password should be more than 8 characters and less than 32 characters</i>");
        } else if(passwordElem.val()!=confirmPasswordElem.val()){
            confirmPasswordElem.parent().addClass("has-error");
            confirmPasswordElem.parent().after("<i class='red'>Password and Confirm Password donot match</i>");
        } else if(! hasAgreed) {
            userAgreement.parent().addClass("has-error");
            userAgreement.parent().after("<i class='red'>Please agree to Terms and Conditions</i>");
        }
    }

    function handleRegisterResponse(data) {
        var formElem = $("form#registerForm");
        if(data.success) {
            formElem.after("<i class='green' style='margin-top: 7px;'>"+data.message+"</i>")
        } else {
            for(var key in data.errors) {
                formElem.find("#"+key).parent().after("<i class='red'>"+data.errors[key]+"</i>");
            }
        }
    }
	// -->
</script>
</div>
</body>
</html>

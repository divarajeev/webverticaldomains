<%--
  Created by IntelliJ IDEA.
  User: Preethivar
  Date: 19/2/14
  Time: 4:20 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Add Category</title>
  <meta name='layout' content='main' />
</head>
<body>
    <g:form controller="secure" action="createCategory">
        <fieldset>
            <div class="clearfix" id="parentCategoryDiv">
                <g:select name="parentCategory" class="mainCategory" from="${com.webverticaldomains.Category.findAllByParentCategoryIsNull()}"
                          optionKey="id" optionValue="name"
                          noSelection="['':'Main']" onchange="catChanged('parentCategory', 'subCatDiv')"/><br/>
                <span id="subCatDiv"></span>
                <input name="actualParentCat" id="actualParentCat" type="hidden" value=""/>
            </div>
            <label class="block clearfix">
                <span class="input-icon input-icon-right">
                    <g:textField class="form-control" placeholder="Name" name="name" />
                    <i class="icon-envelope"></i>
                </span>
            </label>
            <div class="clearfix">
                <button type="submit" class="btn btn-sm btn-success">
                    Create
                    <i class="icon-arrow-right icon-on-right"></i>
                </button>
            </div>
        </fieldset>
    </g:form>
<g:javascript>
    function catChanged(selectId, divId) {
        var catID = $("#"+selectId).val();
        if(catID!="" && catID!=null)
            $("#actualParentCat").val(catID);
        else
            $("#actualParentCat").val($("#"+selectId).parent().parent().find('select').val());
        if(catID != '' && catID != null) {
            $.ajax({
                url: "${createLink(controller: 'secure', action: 'createSubCat')}",
                type:"post",
                data: {catId: catID},
                success: function(data) {
                    if(data != '' && data != null) {
                        $("#"+divId).empty();
                        $("#"+divId).append(data);
                    } else {
                        $("#"+divId).empty();
                    }
                },
                error: function(xhr){
                    alert(xhr.responseText);
                }
            });
        } else  $("#"+divId).empty();
    }
    function registerCategoryOnChange(selectId, divId) {
        $("#"+selectId).on('change', function(){
            if($(this).val()!="" && $(this).val()!=null)
                $("#actualParentCat").val($(this).val());
            else
                $("#actualParentCat").val($(this).parent().parent().find('select').val());
            var divId = $(this).parent().find('div').attr('id');
            catChanged($(this).attr('id'), divId);
        });
    }
</g:javascript>
</body>
</html>
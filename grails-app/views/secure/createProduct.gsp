<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Create Product Page</title>
    <meta name="layout" content="secure_main"/>
</head>
<body>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="widget-box transparent invoice-box">
            <div class="widget-header widget-header-large">
                <h3 class="grey lighter pull-left position-relative">
                    <i class="icon-leaf green"></i>
                    Create Product
                </h3>
            </div>
            <div class="widget-body">
                <div class="widget-main padding-24">
                    <div class="row">
                        <g:if test="${flash.error}">
                            ${flash.error}
                        </g:if>
                        <g:form action="saveProduct" method="post" name="createProduct"
                                enctype="multipart/form-data" onsubmit="return checkCategorySelected()">
                            <div class="form-group" style="float: none; display: block; clear: both; padding: 1px 0px;">
                                <label class="col-sm-3 control-label no-padding-right">Category:</label>
                                <div class="col-sm-9">
                                    <g:select name="parentCategory" class="mainCategory" from="${com.webverticaldomains.Category.findAllByParentCategoryIsNull()}"
                                              optionKey="id" optionValue="name"
                                              noSelection="['':'Select']" onchange="catChanged('parentCategory', 'subCatDiv')"/><br/>
                                    <span id="subCatDiv"></span>
                                    <input name="actualParentCat" id="actualParentCat" type="hidden" value=""/>
                                </div>
                            </div>
                            <div class="form-group" style="float: none; display: block; clear: both; padding: 1px 0px;">
                                <label class="col-sm-3 control-label no-padding-right">Name:</label>
                                <div class="col-sm-9">
                                    <g:textField name="name" class='input-large text_' placeholder="Product Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Price:</label>
                                <div class="col-sm-9">
                                    <g:textField name="price" class='input-large text_' placeholder="Product Price" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Reduced Price(Optional):</label>
                                <div class="col-sm-9">
                                    <g:textField name="reducedPrice" class='input-large text_' placeholder="Product Reduced Price" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Manufacturer:</label>
                                <div class="col-sm-9">
                                    <g:textField name="manufacturer" class='input-large text_' placeholder="Manufacturer of Product" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Brief Description:</label>
                                <div class="col-sm-9">
                                    <g:textField name="briefDesc" class='input-large text_' placeholder="Product Brief Description" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Detailed Description:</label>
                                <div class="col-sm-9">
                                    <g:textArea name="detailDesc" class='input-large text_' placeholder="Product Detailed Description" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Image Files(max 5):</label>
                                <div class="col-sm-9">
                                    <input type="file" accept="image/*" name="productImages" id="productImages" multiple="true" max="5" class='input-large file_' placeholder="Product Images" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"></label>
                                <div class="col-sm-9">
                                    <g:submitButton name="save" value="Save" class="btn btn-info btn-sm">
                                        <i class="icon-key bigger-110"></i>
                                        Save
                                    </g:submitButton>
                                </div>
                            </div>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .form-group {
            float: none !important;
            display: block;
            clear: both !important;
            padding: 1px 0px;
        }
        .form-group:after, .form-group:before {
            clear: both;
            content: "";
            display: block;
        }
    </style>
    <g:javascript>
        function catChanged(selectId, divId) {
            var catID = $("#"+selectId).val();
            if(catID!="" && catID!=null)
                $("#actualParentCat").val(catID);
            else
                $("#actualParentCat").val($("#"+selectId).parent().parent().find('select').val());
            if(catID != '' && catID != null) {
                $.ajax({
                    url: "${createLink(controller: 'secure', action: 'createSubCat')}",
                    type:"post",
                    data: {catId: catID},
                    success: function(data) {
                        if(data != '' && data != null) {
                            $("#"+divId).empty();
                            $("#"+divId).append(data);
                        } else {
                            $("#"+divId).empty();
                        }
                    },
                    error: function(xhr){
                        alert(xhr.responseText);
                    }
                });
            } else  $("#"+divId).empty();
        }
        function registerCategoryOnChange(selectId, divId) {
            $("#"+selectId).on('change', function(){
                if($(this).val()!="" && $(this).val()!=null)
                    $("#actualParentCat").val($(this).val());
                else
                    $("#actualParentCat").val($(this).parent().parent().find('select').val());
                var divId = $(this).parent().find('div').attr('id');
                catChanged($(this).attr('id'), divId);
            });
        }
        function checkCategorySelected() {
            if($("#actualParentCat").val() == '' || $("#actualParentCat").val() == null) {
                alert("Select a category");
                return false;
            } else return true;
        }
    </g:javascript>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Add Category</title>
  <meta name='layout' content='secure_main' />
</head>
<body>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <g:if test="$flash.message">
            <div style="text-align: center">
                <span class="label label-success">${flash.message}</span>
            </div>
        </g:if>
        <div class="widget-box transparent invoice-box">
            <div class="widget-header widget-header-large">
                <h3 class="grey lighter pull-left position-relative">
                    <i class="icon-leaf green"></i>
                    Create Category
                </h3>
            </div>
            <div class="widget-body">
                <div class="widget-main padding-24">
                    <div class="row">
                        <g:if test="${flash.error}">
                            ${flash.error}
                        </g:if>
                        <g:form controller="secure" action="saveCategory" enctype="multipart/form-data">
                            <div class="form-group" style="float: none; display: block; clear: both; padding: 1px 0px;">
                                <label class="col-sm-3 control-label no-padding-right">Parent Category:</label>
                                <div class="col-sm-9">
                                    <g:select name="parentCategory" class="mainCategory" from="${com.webverticaldomains.Category.findAllByParentCategoryIsNull()}"
                                              optionKey="id" optionValue="name"
                                              noSelection="['':'Main']" onchange="catChanged('parentCategory', 'subCatDiv')"/><br/>
                                    <span id="subCatDiv"></span>
                                    <input name="actualParentCat" id="actualParentCat" type="hidden" value=""/>
                                </div>
                            </div>
                            <div class="form-group" style="float: none; display: block; clear: both; padding: 1px 0px;">
                                <label class="col-sm-3 control-label no-padding-right">Name:</label>
                                <div class="col-sm-9">
                                    <g:textField class="form-control" placeholder="Name" name="name" />
                                </div>
                            </div>
                            <div class="form-group" style="float: none; display: block; clear: both; padding: 1px 0px;">
                                <label class="col-sm-3 control-label no-padding-right"> </label>
                                <div class="col-sm-9">
                                    <g:submitButton name="save" value="Save" class="btn btn-info btn-sm">
                                        <i class="icon-key bigger-110"></i>
                                        Save
                                    </g:submitButton>
                                </div>
                            </div>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
    .form-group:after, .form-group:before {
        clear: both;
        content: "";
        display: block;
    }
    </style>
<g:javascript>
    function catChanged(selectId, divId) {
        var catID = $("#"+selectId).val();
        if(catID!="" && catID!=null)
            $("#actualParentCat").val(catID);
        else
            $("#actualParentCat").val($("#"+selectId).parent().parent().find('select').val());
        if(catID != '' && catID != null) {
            $.ajax({
                url: "${createLink(controller: 'secure', action: 'createSubCat')}",
                type:"post",
                data: {catId: catID},
                success: function(data) {
                    if(data != '' && data != null) {
                        $("#"+divId).empty();
                        $("#"+divId).append(data);
                    } else {
                        $("#"+divId).empty();
                    }
                },
                error: function(xhr){
                    alert(xhr.responseText);
                }
            });
        } else  $("#"+divId).empty();
    }
    function registerCategoryOnChange(selectId, divId) {
        $("#"+selectId).on('change', function(){
            if($(this).val()!="" && $(this).val()!=null)
                $("#actualParentCat").val($(this).val());
            else
                $("#actualParentCat").val($(this).parent().parent().find('select').val());
            var divId = $(this).parent().find('div').attr('id');
            catChanged($(this).attr('id'), divId);
        });
    }
</g:javascript>
</body>
</html>
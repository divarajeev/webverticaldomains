<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Admin Pages</title>
  <meta name='layout' content='secure_main' />
</head>
<body>
<div class="col-sm-10 col-sm-offset-1">
    <div class="widget-box transparent invoice-box">
        <div class="widget-header widget-header-large">
            <h3 class="grey lighter pull-left position-relative">
                <i class="icon-leaf green"></i>
                Admin Pages
            </h3>
        </div>
        <div class="widget-body">
            <div class="widget-main padding-24">
                <div class="row">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <g:link action="enterCategory">Create Category</g:link><br /><br />
                        <g:link action="createProduct">Create Product</g:link>
                    </sec:ifAnyGranted>
                    <sec:ifNotGranted roles="ROLE_ADMIN">security access only</sec:ifNotGranted>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<g:select name="subCategory${catId}" class="category" from="${subCats}" optionKey="id" optionValue="name" noSelection="['':'Select']"/>
<div id="subCatDiv${catId}"></div>
<script>
    registerCategoryOnChange("subCategory${catId}", "subCatDiv${catId}");
</script>
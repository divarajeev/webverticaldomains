<%@ page import="grails.plugin.springsecurity.LoginController" contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Please Register</title>
</head>
<body>
    <g:if test="${flash.error}">
        ${flash.error}
    </g:if>
    <g:formRemote name="register" url="${[action: 'registerUser']}">
        Username<input type="text" name="username"><br />
        Password<input type="password" name="password"><br />
        Email Address<input type="text" name="email"><br />
        <g:submitButton name="submit" value="Submit" />
    </g:formRemote>
</body>
</html>
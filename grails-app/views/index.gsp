<%@ page import="com.webverticaldomains.Image; com.webverticaldomains.Product" %>
<!doctype html>
<html>
<head>
    <title>Welcome to the Store</title>
    <meta name="layout" content="main"/>
</head>

<body>
    <div class="center_title_bar" style="width:96%">Latest Products</div>
    <div class="prod_box">
        <g:each in="${products}" var="product">
            <div class="top_prod_box"></div>
            <div class="center_prod_box">
                <div class="product_title"><a href="${createLink(controller: 'product', action: 'details', params: [id: product.id])}" id="${product.name}">${product.name}</a></div>
                <div class="product_img">
                    <g:if test="${product?.images?.asList()?.size() > 0}">
                    <a href="${createLink(controller: 'product', action: 'details', params: [id: product.id])}">
                        <img src="${createLink(controller: 'product', action: 'productImage', params:[imageId: product?.images?.asList()?.first()?.id])}" alt="" border="0" style="max-height: 75px; max-width: 75px;" /></a>
                    </g:if>
                </div>
                <div class="prod_price"><g:if test="${product.reducedPrice}"><span class="reduce">${product.price}$</span> </g:if><span class="price">${product.reducedPrice?:product.price}$</span></div>
                <div class="bottom_prod_box"></div>
                <div class="prod_details_tab">
                    <a href="#" title="header=[Add to cart] body=[&nbsp;] fade=[on]"><img src="images/cart.gif" alt="" border="0" class="left_bt" /></a>
                    <a href="#" title="header=[Specials] body=[&nbsp;] fade=[on]"><img src="images/favs.gif" alt="" border="0" class="left_bt" /></a>
                    <a href="#" title="header=[Gifts] body=[&nbsp;] fade=[on]"><img src="images/favorites.gif" alt="" border="0" class="left_bt" /></a>
                    <a href="${createLink(controller: 'product', action: 'details', params: [id: product.id])}" class="prod_details">details</a>
                </div>
            </div>
        </g:each>
    </div>
   </body>
</html>
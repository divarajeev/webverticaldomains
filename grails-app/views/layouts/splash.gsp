<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- basic styles -->

    <link href="${resource(dir: 'assets/css', file: 'bootstrap.min.css')}" rel="stylesheet"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'font-awesome.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'custom.css')}"/>

    <!--[if IE 7]>
		  <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'font-awesome-ie7.min.css')}" />
		<![endif]-->

    <!-- page specific plugin styles -->
    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery-2.0.3.min.js')}'>" + "<" + "/script>");
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery.validate.min.js')}'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!-- fonts -->

    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace-fonts.css')}"/>

    <!-- ace styles -->

    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace-skins.min.css')}"/>

    <!--[if lte IE 8]>
		  <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace-ie.min.css')}" />
		<![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    %{--<g:javascript src="${resource(dir: 'assets/js', file: 'ace-extra.min.js')}"/>--}%
    <script type="text/javascript" src="${resource(dir: 'assets/js', file: 'ace-extra.min.js')}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    %{--<g:javascript src="${resource(dir: 'assets/js', file: 'html5shiv.js')}"/>
    <g:javascript src="${resource(dir: 'assets/js', file: 'respond.js')}"/>--}%
    <script type="text/javascript" src="${resource(dir: 'assets/js', file: 'html5shiv.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'assets/js', file: 'respond.min.js')}"></script>

    <![endif]-->
    <g:layoutHead/>
    <r:layoutResources/>
</head>
<body>

<g:layoutBody/>
</body>
</html>
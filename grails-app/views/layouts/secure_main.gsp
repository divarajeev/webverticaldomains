<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>

    <meta charset="utf-8"/>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    %{--<link rel="icon" type="image/ico" href="${resource(dir: 'assets/img', file:'favicon.ico')}">--}%
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <!--[if !IE]> -->
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery-2.0.3.min.js')}'>" + "<" + "/script>");
    </script>
    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery-1.10.2.min.js')}'>"+"<"+"/script>");
    </script>
    <script src="${resource(dir: 'assets/js', file: 'respond.src.js')}"></script>
    <![endif]-->

    <script src="${resource(dir: 'assets/js', file: 'upload.js')}"></script>

    <!--[if lt IE 8]>
    <script src="${resource(dir: 'assets/js', file: 'respond.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'excanvas.min.js')}"></script>
	<script src="${resource(dir: 'assets/js', file: 'jquery.gritter.min.js')}"></script>
	<script src="${resource(dir: 'assets/js', file: 'jquery.dataTables.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.dataTables.bootstrap.js')}"></script>
    <![endif]-->

    <link rel="stylesheet" href="${resource(dir: 'assets/js/external/google-code-prettify', file: 'prettify.css')}"/>
    <script src="${resource(dir: 'assets/js/external', file: 'jquery.hotkeys.js')}"></script>

    <script src="${resource(dir: 'assets/js/external/google-code-prettify', file: 'prettify.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'assets/js/external', file: 'index.css')}"/>
    <script src="${resource(dir: 'assets/js/external', file: 'bootstrap-wysiwyg.js')}"></script>

    <link href="${resource(dir: 'assets/css', file: 'bootstrap.min.css')}" rel="stylesheet"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'font-awesome.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'custom.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'chosen.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'jquery.gritter.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'jquery-ui-1.10.3.custom.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'upload.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'daterangepicker-bs3.css')}"/>
    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='${resource(dir: 'assets/js', file: 'jquery.mobile.custom.min.js')}'>" + "<" + "/script>");
    </script>

    <script src="${resource(dir: 'assets/js', file: 'bootstrap.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'typeahead-bs2.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.slimscroll.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.sparkline.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'moment.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'daterangepicker.js')}"></script>
    <script src="${resource(dir: 'assets/js/flot', file: 'jquery.flot.min.js')}"></script>
    <script src="${resource(dir: 'assets/js/flot', file: 'jquery.flot.pie.min.js')}"></script>
    <script src="${resource(dir: 'assets/js/flot', file: 'jquery.flot.resize.min.js')}"></script>



    <!--[if IE 7]>
	<link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'font-awesome-ie7.min.css')}" />
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file:'font-awesome-ie7.min.css')}"/>
	<![endif]-->

    <!--[if !IE]> -->
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery-2.0.3.min.js')}'>" + "<" + "/script>");
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery.validate.min.js')}'>" + "<" + "/script>");
    </script>

    <script src="${resource(dir: 'assets/js', file: 'jquery-ui-1.10.3.custom.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'chosen.jquery.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.inputlimiter.1.3.1.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.autosize.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.maskedinput.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'bootbox.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.dataTables.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.dataTables.bootstrap.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'fuelux/fuelux.wizard.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'fuelux/fuelux.spinner.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'select2.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.gritter.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'ace-extra.min.js')}"></script>

    <!-- ace scripts -->
    <script src="${resource(dir: 'assets/js', file: 'ace-elements.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'ace.min.js')}"></script>
    <!-- <![endif]-->

    <!-- fonts -->
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace-fonts.css')}"/>
    <!-- ace styles -->

    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace-skins.min.css')}"/>

    <!--[if lte IE 8]>
	 <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace-ie.min.css')}" />
	 <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'customie8.css')}" />
	<![endif]-->

    <!--[if lte IE 9]>
    <script src="${resource(dir: 'assets/js', file: 'respond.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'excanvas.min.js')}"></script>
	<script src="${resource(dir: 'assets/js', file: 'jquery.gritter.min.js')}"></script>
	<script src="${resource(dir: 'assets/js', file: 'jquery.dataTables.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.dataTables.bootstrap.js')}"></script>
	<link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'ace-ie.min.css')}" />
	<link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'customie8.css')}" />
	<![endif]-->

    <!--[if !IE]> -->
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery-2.0.3.min.js')}'>"+"<"+"/script>");
    </script>
    <!-- <![endif]-->

    <script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='${resource(dir: 'assets/js', file: 'jquery.mobile.custom.min.js')}'>"+"<"+"/script>");
    </script>
    <script src="${resource(dir: 'assets/js', file: 'bootstrap.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'typeahead-bs2.min.js')}"></script>

    <!-- page specific plugin scripts -->
    <script src="${resource(dir: 'assets/js/fuelux', file: 'fuelux.wizard.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.validate.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'additional-methods.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'bootbox.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.maskedinput.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'select2.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.easy-pie-chart.min.js')}"></script>

    <!-- ace scripts -->
    <script src="${resource(dir: 'assets/js', file: 'ace-elements.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'ace.min.js')}"></script>
    <!--[if !IE]> -->
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${resource(dir: 'assets/js', file: 'jquery-2.0.3.min.js')}'>" + "<" + "/script>");
    </script>
    <!-- <![endif]-->

    <script type="text/javascript">
        if ("ontouchend" in document) document.write("<script src='${resource(dir: 'assets/js', file: 'jquery.mobile.custom.min.js')}'>" + "<" + "/script>");
    </script>
    <script src="${resource(dir: 'assets/js', file: 'bootstrap.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'typeahead-bs2.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.slimscroll.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.sparkline.min.js')}"></script>
    <script src="${resource(dir: 'assets/js/flot', file: 'jquery.flot.min.js')}"></script>
    <script src="${resource(dir: 'assets/js/flot', file: 'jquery.flot.pie.min.js')}"></script>
    <script src="${resource(dir: 'assets/js/flot', file: 'jquery.flot.resize.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'jquery.form.js')}"></script>
		<g:layoutHead/>
        <r:layoutResources />
	</head>
	<body style="background:white">

    <div class="navbar navbar-default" id="navbar">
        <script type="text/javascript">
            try {
                ace.settings.check('navbar', 'fixed')
            } catch (e) {
            }
        </script>

        <div class="navbar-container" id="navbar-container">

            <div class="navbar-header pull-left">
                <a href="#" class="navbar-brand">
                    <small>
                       Store Manager
                    </small>
                </a><!-- /.brand -->
            </div><!-- /.navbar-header -->

        %{--Dummy link to call chnage password--}%
            <a href="#modal-change-password" role="button" class="green" data-backdrop="static" data-keyboard="false" data-toggle="modal" id="callChangePassword" style="display:none"> Table Inside a Modal Box </a>
            <a href="#modal-change-ba" role="button" class="green" data-backdrop="static" data-keyboard="false" data-toggle="modal" id="callChangeBA" style="display:none"> Table Inside a Modal Box </a>


            <div class="navbar-header pull-right" role="navigation">
                <ul class="nav ace-nav">

                    %{--<li class="green">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="icon-shopping-cart"></i>
                            <span class="badge badge-success" id="newMessageBadge">2</span>
                        </a>

                        <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close" id="notifications">
                            <li class="dropdown-header">
                                <i class="icon-shopping-cart" id="newMessageCount"></i>
                                Products in Cart
                            </li>

                            <li>
                                <a href="#">
                                    <img src="http://img8a.flixcart.com/www/promos/new/20131107-154437-20130925-182216-vn-electronics4.png" class="msg-photo" alt="Alex's Avatar">
                                    <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">Nikon Camera HD</span>  <br />
                                            Ciao sociis natoque penatibus et auctor ...
                                        </span>

                                        <span class="msg-time">
                                            <span>Price: 21,000</span>
                                        </span>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="http://img6a.flixcart.com/www/promos/new/20131107-154437-20130925-182216-vn-electronics5.png" class="msg-photo" alt="Alex's Avatar">
                                    <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">Micro SD Card</span> <br />
                                            Ciao sociis natoque penatibus et auctor ...
                                        </span>

                                        <span class="msg-time">
                                            <span>Price: 7,000</span>
                                        </span>
                                    </span>
                                </a>
                            </li>



                            <li>
                                <a href="${createLink(controller: 'project', action: 'buyProduct')}">
                                    Checkout
                                    <i class="icon-arrow-right"></i>
                                </a>
                            </li>
                        </ul>

                    </li>--}%


                    <li class="light-blue" style="width: 200px;">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <span class="user-info" style="max-width: 150px">
                                <small>Welcome,</small>
                                ${org.springframework.security.core.context.SecurityContextHolder.context.authentication.principal.username}
                            </span>

                            <i class="icon-caret-down"></i>
                        </a>


                        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                            <li>
                                <a href="${createLink(controller: 'project', action: 'dashBoard')}" id="dashboardBtn">
                                    <i class="icon-dashboard"></i>
                                    Dashboard
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)" id="changePassword">
                                    <i class="icon-edit"></i>
                                    Change Password
                                </a>
                            </li>

                            <sec:ifAnyGranted roles="ROLE_ADMIN">
                                <li>
                                    <a href="#modal-change-ba" role="button" data-backdrop="static" data-keyboard="false" data-toggle="modal" id="settings">
                                        <i class="icon-cog"></i>
                                        Settings
                                    </a>
                                </li>
                            </sec:ifAnyGranted>

                            <li>
                                <a href="javascript:void(0)">
                                    <i class="icon-flag"></i>
                                    Version: <g:meta name="app.version"/>
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <g:form controller="logout" name="logoutForm" method="POST">

                                </g:form>
                                <a href="#" onclick="jQuery('#logoutForm').submit();">
                                    <i class="icon-off"></i>
                                    Logout
                                </a>

                            </li>
                        </ul>
                    </li>
                </ul><!-- /.ace-nav -->
            </div><!-- /.navbar-header -->
        </div><!-- /.container -->
    </div>


    <g:layoutBody/>
	<g:javascript library="application"/>
        <r:layoutResources />
	</body>
</html>
<div class="row">
    <div class="" role="main" style="width:90%; margin:auto;margin-top:50px;">
        <div id="${carouselId}" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#${carouselId}" data-slide-to="0" class="active"></li>
                <li data-target="#${carouselId}" data-slide-to="1"></li>
                <li data-target="#${carouselId}" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="http://4images.in/wp-content/uploads/2013/12/Siberian-Tiger-Running-Through-Snow-Tom-Brakefield-Getty-Images-200353826-001.jpg" alt="...">
                    <div class="carousel-caption">
                        Tiger
                    </div>
                </div>
                <div class="item">
                    <img src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRr0WK-Q2t4Xxr1b6Kl7-lXdVEIh_Hj3HiDXk--Qg_0UAY0Y96P6w" alt="...">
                    <div class="carousel-caption">
                        Flowers
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#${carouselId}" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#${carouselId}" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>

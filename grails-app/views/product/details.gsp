<!doctype html>
<html>
<head>
    <title>Product Details</title>
    <meta name="layout" content="main"/>
</head>
<body>
<div class="center_title_bar"><div class="product_title_big">${product.name}</div></div>
<div class="prod_box_big">
    <div class="top_prod_box_big"></div>
    <div class="center_prod_box_big">
        <div class="product_img_big">
            <a class="gal" href="${createLink(controller: 'product', action: 'productImage', params:[imageId: product.images?.asList()?.first()?.id])}"><img src="${createLink(controller: 'product', action: 'productImage', params:[imageId: product.images?.asList()?.first()?.id])}" alt="" border="0" style="max-height: 100px; max-width: 100px;" /></a>
            <div class="thumbs">
                <g:each in="${product.images?.asList()?:[]}" var="imageInstance" status="i">
                    <g:if test="${i!=0}"><a class="gal" href="${createLink(controller: 'product', action: 'productImage', params:[imageId: imageInstance?.id])}"><img src="${createLink(controller: 'product', action: 'productImage', params:[imageId: imageInstance?.id])}" alt="" border="0" style="max-height: 30px; max-width: 30px;"/></a></g:if>
                </g:each>
            </div>
        </div>
        <div class="details_big_box">
            <div class="specifications">
                ${product.detailDesc}<br />
                <span class="blue">${product.briefDesc}</span><br />
                %{--<span class="blue"></span><br />
                <span class="blue"></span><br />
                <span class="blue"></span><br />--}%
            </div>
            <div class="prod_price_big">
                <g:if test="${product.reducedPrice}">
                 <span class="reduce">${product.price}$</span></g:if><span class="price">${product.reducedPrice?:product.price}$</span>
            </div>
            <div class="profile-contact-links">
            <g:remoteLink  controller="product" action="addToCart" params="[id: product.id , quantity: 1]" class="addToCart" >ADD TO CART</g:remoteLink>

            </div>
        </div>
        <div class="bottom_prod_box_big"></div>
    </div>
</div>
<div class="center_title_bar">Similar products</div>
    <div class="prod_box">
    <div class="top_prod_box"></div>
</div>
<div class="center_title_bar">Reviews </div>
<div class="prod_box">
    <div class="top_prod_box">
        <g:each in="${product.reviews}" var="review">
            <p>Review By : ${review.createdBy}</p>
            <p>Rating : ${review.rating}</p>
            <p>Comment : ${review.comment}</p>
            <hr/>
        </g:each>
        <g:if test="${eligibleForReview}">
            <g:form controller="product" action="addReview">
                <g:hiddenField name="productId" value="${product.id}"/>
                <p>Rating <g:select name="rating" from="${1..5}"/></p>
                <p>Comment <g:textArea name="comment"/></p>
                <g:submitButton name="addReviewButton" class="btn btn-primary" value="Add Review"/>
            </g:form>
        </g:if>
    </div>
</div>
<script type="text/javascript">
    if($.colorbox) {
        //$("a[title^='header=[Zoom'], a[title^='header=[Thumb']").colorbox({rel: 'gal', photo: true, scalePhotos: true, maxHeight: (window.innerHeight * 0.8), maxWidth: (window.innerWidth * 0.8)});
        $("a.gal").colorbox({rel: 'gal', photo: true, scalePhotos: true, maxHeight: (window.innerHeight * 0.8), maxWidth: (window.innerWidth * 0.8)});
    }
</script>
</body>
</html>
<%@ page import="com.webverticaldomains.Image; com.webverticaldomains.Product" %>
<!doctype html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta name="layout" content="main"/>
    </head>
  <body>

    <div class="center_title_bar">The following are the items in your cart</div></br></br>
   <g:each in="${products}" var="productInfo">
      <div class="product_title">${productInfo.product.name}</div>
        <div class="product_img"> <a href="#" title="header=[Thumb1] body=[&nbsp;] fade=[on]"><img src="${createLink(action: 'productImage', params: [productId: productInfo.product.id, imageId: productInfo.product.images?.asList()?.first().id])}" alt="" border="0" height="100" width="100"/></a></div>
          <div class="specifications"> Quantity: ${productInfo.quantity}</div>
             <div class="specifications">
                <g:if test="${productInfo.product.reducedPrice}">
                <span class="reduce">${productInfo.product.price}</span></g:if><span class="price">${productInfo.product.reducedPrice?:productInfo.product.price}</span>
             </div>
              </g:each>
  </body>
</html>
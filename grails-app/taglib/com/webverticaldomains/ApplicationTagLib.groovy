package com.webverticaldomains

class ApplicationTagLib {

    def carousel = { attrs, body ->
        if(!attrs.id && !attrs.name) {
            throw new RuntimeException("name and id both cannot be null")
        }
        def id = attrs.id ?: attrs.name
        out << render(template: '/taglibs/carousel', model: [carouselId: id])
    }
}

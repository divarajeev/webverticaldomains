package com.webverticaldomains

class Image {

    String name
    byte[] file
    String type

    static constraints = {
        file(nullable: false, maxSize: 1000000)
    }
}

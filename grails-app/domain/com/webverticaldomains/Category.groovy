package com.webverticaldomains

class Category {

    String name
    Date dateCreated
    Category parentCategory

    static constraints = {
        name(nullable: false, blank: false)
        parentCategory(nullable: true, blank: true)
    }
}

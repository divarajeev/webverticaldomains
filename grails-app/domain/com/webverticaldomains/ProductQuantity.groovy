package com.webverticaldomains

class ProductQuantity {

    Product product
    int quantity
    double amount

    static belongsTo = [bill: Billing]

    static constraints = {
        quantity(nullable: false)
        bill(nullable: false, unique: ['product'])
    }
}

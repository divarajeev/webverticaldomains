package com.webverticaldomains

class Product {

    String name
    double price
    double reducedPrice
    Date dateCreated
    int available
    String briefDesc
    String detailDesc
    String tag
    String manufacturer

    static belongsTo = [category: Category]

    static hasMany = [images: Image, reviews: Review]

    static constraints = {
        name(nullable: false, blank: false)
        briefDesc(nullable: false, blank: false)
        detailDesc(nullable: false, blank: false, maxSize: 2500)
        tag(nullable: true, blank: true)
        reducedPrice(nullable: true)
        reviews(nullable: true)
    }

    static mapping = {
        images cascade: 'all-delete-orphan'
        reviews cascade: 'all-delete-orphan'
    }
}

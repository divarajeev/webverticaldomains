package com.webverticaldomains

import com.webverticaldomains.auth.User

class Review {

    transient springSecurityService

    String comment
    int rating
    //Billing bill
    User createdBy
    Date dateCreated

    static belongsTo = [product: Product]
    static transients = ['springSecurityService']

    static constraints = {
        comment blank: false, nullable: false, maxSize: 10000
        createdBy nullable: true
        rating range: 1..5
        /*bill(nullable: false, blank: false, unique: true, validator: { val, obj ->
            if (val.createdBy != springSecurityService.currentUser)
                ['Invalid bill, doesn\'t belong to user']
        })*/
    }

    def beforeInsert() {
        this.createdBy = springSecurityService.getCurrentUser()
    }
}

package com.webverticaldomains

import com.webverticaldomains.auth.User

class Billing {

    transient springSecurityService

    Date dateCreated
    User createdBy
    double total
    Date delivered
    Date estimatedDelivery
    String shippingAddress
    String deliveryStatus
    PaymentMethod paymentMethod
    String paymentDetails

    static hasMany = [productQuantities: ProductQuantity] //[products: Product]
    static transients = ['springSecurityService']

    static constraints = {
        delivered(nullable: true)
        estimatedDelivery(nullable: false)
        shippingAddress(blank: false, nullable: false, maxSize: 500)
        deliveryStatus(blank: false, nullable: false)
        paymentMethod(blank: false, nullable: false)
        paymentDetails(blank: false, nullable: false)
    }

    def beforeInsert() {
        this.createdBy = springSecurityService.getCurrentUser()
    }
}
